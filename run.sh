#!/bin/sh
set +o xtrace
if ! type keytool &>/dev/null; then
	error "keytool missing, java is not installed properly"
else
	info "keytool is installed"
	debug "$(java -version)"
fi

if [ ! -n "$WERCKER_KEYTOOL_FILE" ]; then
	WERCKER_KEYTOOL_FILE="cert.pem"
fi

if [ ! -n "$WERCKER_KEYTOOL_KEYSTORE" ]; then
	WERCKER_KEYTOOL_KEYSTORE="/etc/ssl/certs/java/cacerts"
fi

if [ ! -n "$WERCKER_KEYTOOL_PASS" ]; then
	WERCKER_KEYTOOL_PASS="changeit"
fi

function disable_xtrace_and_return_status() {
    set +o xtrace
    return $1
}

function run() {
    if [ -n "$WERCKER_KEYTOOL_DEBUG" ]; then
        set -o xtrace
    fi

    keytool -importcert -noprompt \
     -keystore $WERCKER_KEYTOOL_KEYSTORE \
     -file $WERCKER_KEYTOOL_FILE \
     -storepass $WERCKER_KEYTOOL_PASS

    disable_xtrace_and_return_status $?
}

run;
